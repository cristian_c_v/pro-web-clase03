from django.shortcuts import render
from proweb.models import Contacto

def load_contacto(request):
    print('mantenedorcontacto.py -> load_contacto')    
    print('method -> ', request.method)
    if request.method == 'GET':
        try:
            codigo = request.GET['codigo']
            print('codigo -> ', codigo)
            contacto = Contacto.objects.get(pk=codigo)
            contacto.delete()
        except Exception as e:
            print(e)
        contactos = Contacto.objects.all
        return render(request, 'mantenedor-contacto.html', {'contactos': contactos})
