from django.shortcuts import render
from proweb.models import Contacto

def load_contacto(request):
    if request.method == 'GET':
        contactos = Contacto.objects.all
        return render(request, 'mantenedor-contacto.html', {'contactos': contactos})
